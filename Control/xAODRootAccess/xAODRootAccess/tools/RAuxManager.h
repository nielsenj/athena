#ifndef XAODROOTACCESS_TOOLS_RAUXMANAGER_H
#define XAODROOTACCESS_TOOLS_RAUXMANAGER_H

// System include(s):
#include <memory>

// Local include(s):
#include "TVirtualManager.h"

// Forward declaration(s):
namespace SG {
   class IConstAuxStore;
}

namespace xAOD {

   // Forward declaration(s):
   class RAuxStore;

   /// @short Manager for RAuxStore objects
   ///
   /// This class is used when connecting RAuxStore objects to the
   /// input ntuple as the auxiliary store of a DV container.
   ///
   class RAuxManager : public TVirtualManager {

   public:
      /// Constructor getting hold of an auxiliary store object
      RAuxManager( RAuxStore* store, ::Bool_t sharedOwner = kTRUE );
      /// Copy constructor
      RAuxManager( const RAuxManager& parent );

      /// Assignment operator
      RAuxManager& operator=( const RAuxManager& rhs );

      /// Function for updating the object in memory if needed
      virtual ::Int_t getEntry( ::Int_t getall = 0 ) override;

      /// Function getting a const pointer to the object being handled
      virtual const void* object() const override;
      /// Function getting a pointer to the object being handled
      virtual void* object() override;
      /// Function replacing the object being handled
      virtual void setObject( void* obj ) override;

      /// Create the object for the current event
      virtual ::Bool_t create() override;
      /// Check if the object was set for the current event
      virtual ::Bool_t isSet() const override;
      /// Reset the object at the end of processing of an event
      virtual void reset() override;

      /// Get a type-specific pointer to the managed object
      RAuxStore* getStore();
      /// Get a convenience pointer to the managed object
      const SG::IConstAuxStore* getConstStore() const;

   private:
      /// The auxiliary store object
      std::shared_ptr< RAuxStore > m_store;
      /// Pointer to the auxiliary store object
      RAuxStore* m_storePtr;

   };  // class RAuxManager

}  // namespace xAOD

#endif  // XAODROOTACCESS_TOOLS_TAUXMANAGER_H
